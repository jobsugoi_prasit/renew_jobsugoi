<?php include("header.php"); ?>
<?php include("nav.php"); ?>
<div class="container	">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p>
					<ul class="list-inline">
						<li>Home ></li>
						<li class="jobseeker-text-jobseeker">For jobseeker</li>
					</ul>
				</p>
				<h2><b>For Jobseeker</b></h2>
			</div>
			<!--  -->
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-7">
				<div class="jumbotron jobseeker-background">
					<div class="container">
						<div class="col-xs-12 col-sm-3 col-md-4 col-lg-9 col-md-offset-1">
						<CENTER><h2>Log in</h2></CENTER>
						<form>
						  <div class="form-group">
						    <label for="exampleInputEmail1" class="jobseeker-text-email">E-mail</label>
						    <input type="email" class="form-control jobseeker-inputtxt" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1" class="jobseeker-text-password">Password</label>
						    <input type="password" class="form-control jobseeker-inputtxt" id="exampleInputPassword1" placeholder="Password">
						  </div>
						  <center>
						  <button type="button" class="btn btn-default btn-md jobseeker-btn-login">Log in</button>
						  </center>
						  <div class="form-group">
						  	<br>
						    <label for="exampleInputPassword1"><a href="#!"><h6>Forgot password</h6></a></label>
						  </div>
						 </form>
					</div>
					<div class="clearfix"></div>
					<hr class="jobseeker-hr-color">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 jobseeker-frame">
						<p>
							<center>
							<p><h4>Log in with Facebook</h4></p>
							<p><button type="button" class="btn btn-default jobseeker-facebook-login">Log in with Facebook</button></p>
							</center>
						</p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 jobseeker-clear-bk">
						<p>
							<center>
							<p><h4>Log in with Line</h4></p>
							<p><button type="button" class="btn btn-default jobseeker-line-login">Log in with LINE</button></p>
							</center>
						</p>
					</div>
					<div class="clearfix">
					</div>
				    <hr class="jobseeker-hr-color">
					<CENTER><a href="#!" style="color:#af2a41;"><h6>For Company click</h6></a></CENTER>
					</div>
				</div>
			</div>
			<div class="col-xs-12	 col-sm-4 col-md-4 col-lg-4 col-md-offset-1">
				<center><h2 style="margin-top:-10px;">Register</h2></center>
				<!-- login -->
				<form>
				  <div class="form-group">
				    <label for="exampleInputEmail1">Firstname</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Please input  your first name">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Lastname</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Please input your last name">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Email</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Please input your email">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Please input your password">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Repeat Password</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Pleae input your password agian">
				  </div>
				  <div class="form-group">
				    <img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/451x107/fe7a7699ff4cd12702e0f93c6c1fcbcc/1562315859450.jpg" class="img-responsive" alt="Image">
				  </div>
					<center>
				  <button type="button" class="btn btn-default btn-md jobseeker-register">Register</button>
				  </center>
				</form>
				<div class="clearfix"></div>
				<center><a href="#!"><h6>For Jobseeker</h6></a></center>
				<!-- login -->
				<div class="clearfix">
				</div>
				<hr>
				<br>
			</div>
			<div class="clearfix"></div>					
		</div>
	</div>
</div>
<?php include("foolter.php"); ?>
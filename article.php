<?php include("header.php"); ?>
<?php include("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p>
					<ul class="list-inline">
						<li>Home ></li>
						<li>Job Search ></li>
						<li>All jobs ></li>
					</ul>
				</p>
				<h2><b>Article</b></h2>
			</div>
			<!--  -->
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
				<img src="https://trello-attachments.s3.amazonaws.com/586ca934a66660198872b023/5d00ca36e55e560f8ae77b0c/3db938051f25c82fb7d6393a2b7ce775/1561711413615.jpg" class="img-responsive" alt="Image">
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<ul class="list-inline">
					<li class="list-inline-item">Update : 14-05-2019</li>
					<li class="list-inline-item">
						<button type="button" class="btn btn-default btn-xs arl-what">What's going on</button>
					</li>
				</ul>
				<h2>What is Lorem lpsum?</h2>
				<div class="clearfix"></div>
				<span>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			   </span>
			   <div class="clearfix"></div>
			   <br>
			   <button type="button" class="btn btn-default btn-sm arl-viewdetail">View detail</button>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 arl-menu">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2"><h3>All</h3></div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2"><h3>What's going on</h3></div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2"><h3>Active</h3></div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2"><h3>Activites</h3></div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2"><h3>Don't miss out</h3></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<!--  -->
					<?php for($i=0;$i<12;$i++) { ?>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<div class="thumbnail">
						<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjQyIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDI0MiAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MjAwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTZiOWRhNDM4NzUgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMnB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNmI5ZGE0Mzg3NSI+PHJlY3Qgd2lkdGg9IjI0MiIgaGVpZ2h0PSIyMDAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI4OS44MDQ2ODc1IiB5PSIxMDUuMSI+MjQyeDIwMDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" alt="..." style="width:100%;">
						<div class="caption">
						<p><span>Update 14:05 2019    What's going</span>&nbsp;&nbsp;<span><button type="button" class="btn btn-default btn-xs arl-viewdetail">What's going on</button></span></p>
						<div class="clearfix"></div>
						<p><B>HEADER</B></p>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
						<p>...</p>
						<p><a href="#" class="btn btn-primary arl-button" role="button">></a></p>
						</div>
						</div>
					</div>
					<?php } ?>
					<!--  -->
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("foolter.php"); ?>
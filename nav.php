<nav class="navbar navbar-default nav-background" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="index.php"><img src="upload/logo.png" class="img-responsive" alt="Image"></a></li>
			</ul>
			<ul class="nav navbar-nav">
				<li><a href="jobs.php"><h3>FIND JOBS</h3></a></li>
				<li><a href="article.php"><h3>ARTICLE</h3></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="jobseeker.php"><h3><button type="button" class="btn btn-default btn-lg nav-btn-jobseeker">For jobseeker</button></h3></a></li>
				<li><a href="company.php"><h3><button type="button" class="btn btn-default btn-lg nav-btn-company">For Company</button></h3></a></li>
				<li><a href="#"><h3><button type="button" class="btn btn-default btn-lg nav-btn-login">LOGIN</button></h3></a></li>
				<li><a href="#"><h3 style="margin-top:30px;"><span class="nav-lang">TH</span></h3></a></li>
				<li class="dropdown" style="display:none;">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"><h3>Action</h3></a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li><a href="#">Separated link</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>
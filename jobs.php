<?php include("header.php"); ?>
<?php include("nav.php"); ?>
<div class="container-fluid jobs-background">
	<div class="row">
		<div class="col-lg-12">
			 <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			 	<h3>Filter Jobs</h3>
			 	<div class="clearfix"></div>
			 	<form>
					<div class="form-group">
						<label for="exampleInputEmail1">Skill & keyword</label>
						<input type="text" class="form-control findjob-fillter-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">				
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">Job function</label>
						<select name="" id="input" class="form-control findjob-fillter-input " required="required">
							<option value=""></option>
						</select>			
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Area</label>
						<select name="" id="input" class="form-control findjob-fillter-input " required="required">
							<option value=""></option>
						</select>
					</div>
					<!--  -->
					<div class="form-group">
						<label for="exampleInputPassword1">Industry</label>
						<select name="" id="input" class="form-control findjob-fillter-input " required="required">
							<option value=""></option>
						</select>
					</div>
					<!--  -->
					<div class=" col-md-6 form-group findjob-salaryrang-from">
						<label for="exampleInputPassword1">Salary Range</label>
						<select name="" id="input" class="form-control col-sm-2 findjob-fillter-input " required="required">
							<option value=""></option>
						</select>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-6 form-group findjob-salaryrang-to">
						<label for="exampleInputPassword1">Salary Range</label>
						<select name="" id="input" class="form-control col-sm-2 findjob-fillter-input " required="required">
							<option value=""></option>
						</select>
						<div class="clearfix"></div>
					</div>
					<!--  -->
					<div class="col-md-6 form-group">
						<label for="exampleInputPassword1"></label>
						<div>
							<label class="checkbox-inline">
							<input type="checkbox" id="inlineCheckbox1" class="findjob-checkbox" value="option1">&nbsp;&nbsp;Entry
							</label>
						</div>
					</div>
					<!--  -->
					<div class="col-md-6 form-group">
						<label for="exampleInputPassword1"></label>
						<div>
							<label class="checkbox-inline">
							<input type="checkbox" id="inlineCheckbox1" class="findjob-checkbox" value="option1">&nbsp;&nbsp;Senior
							</label>
						</div>
					</div>
					<!--  -->
					<div class="col-md-6 form-group">
						<label for="exampleInputPassword1"></label>
						<div>
							<label class="checkbox-inline">
							<input type="checkbox" id="inlineCheckbox1" class="findjob-checkbox" value="option1">&nbsp;&nbsp;Middle
							</label>
						</div>
					</div>
					<!--  -->
					<div class="col-md-6 form-group">
						<label for="exampleInputPassword1"></label>
						<div>
							<label class="checkbox-inline">
							<input type="checkbox" id="inlineCheckbox1" class="findjob-checkbox" value="option1">&nbsp;&nbsp;Executive
							</label>
						</div>
					</div>
					<div class="clearfix">
					
					</div>
					<hr>
					<button type="submit" class="btn btn-primary btn-lg findjob-search"><span class="glyphicon glyphicon-search"></span> SEARCH</button>
				</form>
			 </div>
			  <div class="col-xs-12 col-sm-9 col-md-10 col-lg-9 jobdeail-background">
			  	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			  		<p>
					<ul class="list-inline">
						<li>Home ></li>
						<li>Job Search ></li>
						<li>All jobs ></li>
					</ul>
					</p>
			  	</div>
			  	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-right">
			  		<p>
			  		<h5>1-10of 200 job positions</h5>
			  		<h5>For job function:Marketing Manager</h5>
			  		</p>
			 	</div>
			  	<div class="jumbotron">
			  		<div class="container">
			  			<h1>IMAGE</h1>
			  			<p>Contents ...</p>	
			  		</div>
			  	</div>
				<!--  -->
				<?php 
				for($i=0;$i<15;$i++) {
				 ?>
				<div class="media">
			      <div class="media-body">
			      	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">

			      		<a href="#">
			          <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/200x190/c1dd2aa9a0a54516c806851f3c2ac0d1/company819.jpg" data-holder-rendered="true">
			        </a>
			      	</div>
			      	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			      		 <h3 class="media-heading"><b>Marketing Manager</b></h3>
						<h4>Company : Central & Matsumoto kiyoshi limited</h4>
						<h4><span class="glyphicon glyphicon-map-marker"></span> BTS Chong Nonsi</h4>
						<h4><span class="glyphicon glyphicon-usd"></span> Not Specified</h4>
						<h4><span class="glyphicon glyphicon-calendar"></span> UPDATE 03/05/2019</h4>
			      	</div>
			      	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
			      		<a href="#">
							<button type="button" class="btn btn-default front-button-more">View jobs ></button>
						</a>
						<br>
						<br>
			      	</div>
			      </div>
			    </div>
			    <div class="clearfix"></div>
				<?php } ?>
			    <!--  -->
			 </div>
		</div>
	</div>
</div>
<?php include("foolter.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="jumbotron" id="jumbotron-top-background">
			<div class="container-fluid">
				<div class="container front-container">
					<div class="jumbotron">
						<div class="container">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<ul class="list-inline">
								<li class="list-inline-item"><b>Search jobs</b></li>
								<li class="list-inline-item"><b>Companies</b></li>
								<li class="list-inline-item"><b>Area</b></li>
								</ul>
								<hr>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
								<input type="text" name="" id="input" class="form-control" value="" required="required" pattern="" title="">
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
								<button type="button" class="btn btn-default">Search</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
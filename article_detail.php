<?php include("header.php"); ?>
<?php include("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p>
					<ul class="list-inline">
						<li>หน้าแรก ></li>
						<li>บทความ ></li>
						<li>กม.คุ้มครองแรงงานฉบับใหม่ เพิ่มสิทธิลา-เพิ่มค่าชดเชยเลิกจ้าง ></li>
					</ul>
				</p>
				<h2><b>บทความ</b></h2>
			</div>
			<!--  -->
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
				<div class="well well-sm arl-detail-well">
					<img src="https://trello-attachments.s3.amazonaws.com/586ca934a66660198872b023/5d00ca36e55e560f8ae77b0c/3db938051f25c82fb7d6393a2b7ce775/1561711413615.jpg" class="img-responsive" alt="Image">
				</div>
			    <div class="clearfix"></div>
			    <div class="well well-sm arl-detail-well">
			    	<span class="text-left">แก้ไขล่าสุด : 14-05-2019</span>
			    	<span style="float:right;"><button type="button" class="btn btn-default btn-sm arl-button">ลูกจ้างควรรู้</button></span>
			    </div>
			    <div class="well well-sm arl-detail-well">
			    	<h2>กม ค้มครองแรงงานฉบับใหม่ เพิ่มสอทิธิลา-เพิ่มค่าชดเชยเลิกจ้าง</h2>
			    	<br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			    	<br>
			    	<br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			    	<br>
			    	<br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			    	<br>
			    	<br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			    	<br>
			    	<br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					<div class="clearfix"></div>
			    </div>
			    <div class="well arl-button">
			    	<center>สำหรับบริษัท &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-default btn-lg arl-detail-contact">ติดต่อเรา</button></center>
			    </div>
			</div>
			<div class="col-xs-12	 col-sm-4 col-md-4 col-lg-4">
				<h2 style="margin-top:-10px;">บทความอื่น</h2>
				<?php for($i=0;$i<10;$i++) { ?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					
					<div class="well well-sm arl-detail-slider">
						<img src="https://trello-attachments.s3.amazonaws.com/586ca934a66660198872b023/5d00ca36e55e560f8ae77b0c/3db938051f25c82fb7d6393a2b7ce775/1561711413615.jpg" class="img-responsive" alt="Image">
						<div class="clearfix"></div>
						<br>
						<span class="text-left">แก้ไขล่าสุด : 14-05-2019</span>
				    	<span style="float:right;"><button type="button" class="btn btn-default btn-sm arl-button">ลูกจ้างควรรู้</button></span>
				    	<div class="clearfix"></div>
						<br>
				    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				    	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				    	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				    	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				    	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				    	<button type="button" class="btn btn-default arl-detail-button-black ">></button>
					</div>
				</div>
				<?php } ?>
				<div class="clearfix">
				</div>
				<hr>
				<center><button type="button" class="btn btn-default btn-sm arl-button">ดูเพิ่มเติม.......</button></center>
				<br>
			</div>
			<div class="clearfix"></div>					
		</div>
	</div>
</div>
<?php include("foolter.php"); ?>
<?php include("header.php"); ?>
<?php include("nav.php"); ?>
<?php include("bannertop.php"); ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front-colum">
			<center>
				<div class="page-header">
				<h2><b>Popular job categories</b></h2>
				</div>
			</center>
			<div class="owl-carousel">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12r">
					<img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/317x240/d6142406964f222f9ef7512638343383/1560334381469.jpg" height="300" alt="">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12r">
					<img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/317x240/d6142406964f222f9ef7512638343383/1560334381469.jpg" height="300" alt="">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12r">
					<img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/317x240/d6142406964f222f9ef7512638343383/1560334381469.jpg" height="300" alt="">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12r">
					<img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/317x240/d6142406964f222f9ef7512638343383/1560334381469.jpg" height="300" alt="">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12r">
					<img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/317x240/d6142406964f222f9ef7512638343383/1560334381469.jpg" height="300" alt="">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12r">
					<img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/317x240/d6142406964f222f9ef7512638343383/1560334381469.jpg" height="300" alt="">
				</div>
				
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front-colum">
			<div class="jumbotron">
				<div class="container">
					<center>
						<div class="page-header">
							<h2><b>Job Categories</b></h2>
						</div>
					</center>
					<br>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
						<div>xxxxxxxxxxxxxxxx</div>
					</div>
				</div>
				<center>
					<div class="page-header">
					<button type="button" class="btn btn-default front-button-more">See more...</button>
					</div>
				</center>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front-colum front-colum-bk">
			<div class="container">
				<center>
					<div class="page-header">
						<h2><b>Companies</b></h2>
					</div>
				</center>
				<div class="col-xs-12 col-sm-3 col-md-4 col-lg-3 text-center">
					<div><img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/200x190/1fa1b9fc8fdaa0421f31e8126eae675e/company646.png" class="img-circle" alt="Image"></div>
					<p><div>xxxxxxxxxxxxxxxx</div></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-4 col-lg-3 text-center">
					<div><img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/200x190/1fa1b9fc8fdaa0421f31e8126eae675e/company646.png" class="img-circle" alt="Image"></div>
					<p><div>xxxxxxxxxxxxxxxx</div></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-4 col-lg-3 text-center">
					<div><img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/200x190/1fa1b9fc8fdaa0421f31e8126eae675e/company646.png" class="img-circle" alt="Image"></div>
					<p><div>xxxxxxxxxxxxxxxx</div></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-4 col-lg-3 text-center">
					<div><img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/200x190/1fa1b9fc8fdaa0421f31e8126eae675e/company646.png" class="img-circle" alt="Image"></div>
					<p><div>xxxxxxxxxxxxxxxx</div></p>
				</div>
			</div>
			<br>
			<center>
				<div class="page-header">	
					<button type="button" class="btn btn-defaul front-button-more">See more...</button>
				</div>
			</center>	
		</div>	
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front-colum">
			<center>
				<div class="page-header">
					<h2><b>How to Apply</b></h2>
				</div>
			</center>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
				<div><img src="https://www.thedayspring.com.pk/wp-content/uploads/2019/01/no-image-available.jpg" class="img-circle" alt="Image"></div>
				<p><div>xxxxxxxxxxxxxxxx</div></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
				<div><img src="https://www.thedayspring.com.pk/wp-content/uploads/2019/01/no-image-available.jpg" class="img-circle" alt="Image"></div>
				<p><div>xxxxxxxxxxxxxxxx</div></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
				<div><img src="https://www.thedayspring.com.pk/wp-content/uploads/2019/01/no-image-available.jpg" class="img-circle" alt="Image"></div>
				<p><div>xxxxxxxxxxxxxxxx</div></p>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front-colum">
			<center>
				<div class="page-header">
					<h2><b>BLOG</b></h2>
				</div>
			</center>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
				<div><img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/316x218/d7d753956916d4617c0cc3407b670a8d/1560334997099.jpg" class="img-responsive" alt="Image"  style="width:100%;"></div>
				<p><div>xxxxxxxxxxxxxxxx</div></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
				<div><img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/316x218/d7d753956916d4617c0cc3407b670a8d/1560334997099.jpg" class="img-responsive" alt="Image" style="width:100%;"></div>
				<p><div>xxxxxxxxxxxxxxxx</div></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
				<div><img src="https://trello-attachments.s3.amazonaws.com/5d00ca36e55e560f8ae77b0c/316x218/d7d753956916d4617c0cc3407b670a8d/1560334997099.jpg" class="img-responsive" alt="Image"  style="width:100%;"></div>
				<p><div>xxxxxxxxxxxxxxxx</div></p>
			</div>
			<div class="clearfix"></div>
			<br>
			<center>
				<div class="page-header">
					<button type="button" class="btn btn-default front-button-more">See more...</button>
				</div>
			</center>
			<br>						
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 front-colum">
			<div class="jumbotron front-subscription-bk">
				<div class="container">
					<h3>Job Subscription</h3>
					<br>
					<form action="" method="POST" class="form-inline" role="form">
						<div class="form-group">
							<span>Subscribe</span>&nbsp;&nbsp;&nbsp;
							<label class="sr-only" for="">label</label>
							<input type="text" class="form-control front-subscribe-input" id="" placeholder="Input field">
						</div>
						<div class="form-group">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Send to</span>&nbsp;&nbsp;&nbsp;
							<label class="sr-only" for="">label</label>
							<input type="text" class="form-control front-subscribe-send" id="" placeholder="Input field">
						</div>
						&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-sm front-subscribe-submit">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("foolter.php"); ?>